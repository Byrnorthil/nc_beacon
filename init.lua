-- LUALOCALS < ---------------------------------------------------------
local math, minetest, nodecore, pairs, string, vector
    = math, minetest, nodecore, pairs, string, vector
local math_ceil, math_log, string_gsub
    = math.ceil, math.log, string.gsub
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local beaconame = modname .. ":beacon"
local beacondesc = "Beacon"
local dntname = modname .. "tick"

local amaldef = minetest.registered_nodes["nc_igneous:amalgam"]
if not amaldef then return end

local recipenodes = {
	{match = "nc_igneous:amalgam"},
	{x = -1, z = -1, match = "nc_lode:block_annealed"},
	{x = -1, match = "nc_lode:block_annealed"},
	{x = -1, z = 1, match = "nc_lode:block_annealed"},
	{x = 1, z = -1, match = "nc_lode:block_annealed"},
	{x = 1, match = "nc_lode:block_annealed"},
	{x = 1, z = 1, match = "nc_lode:block_annealed"},
	{z = -1, match = "nc_lode:block_annealed"},
	{z = 1, match = "nc_lode:block_annealed"},
	{x = -1, y = 1, z = -1, match = "nc_lux:flux_flowing"},
	{x = -1, y = 1, z = 1, match = "nc_lux:flux_flowing"},
	{x = 1, y = 1, z = -1, match = "nc_lux:flux_flowing"},
	{x = 1, y = 1, z = 1, match = "nc_lux:flux_flowing"},
	{x = -1, y = 1, match = {
			name = "nc_writing:glyph2",
			param2 = 3
	}},
	{x = 1, y = 1, match = {
			name = "nc_writing:glyph2",
			param2 = 1
	}},
	{y = 1, z = -1, match = {
			name = "nc_writing:glyph2",
			param2 = 2
	}},
	{y = 1, z = 1, match = {
			name = "nc_writing:glyph2",
			param2 = 0
	}},
	{y = 1, match = "air", replace = beaconame},
}

------------------------------------------------------------------------
-- Beacon Database

local allpos, setpos, rmpos
do
	local modstore = minetest.get_mod_storage()
	local db = modstore:get_string("data")
	db = db and minetest.deserialize(db) or {}
	allpos = function() return pairs(db) end
	local function savedb()
		return modstore:set_string("data", minetest.serialize(db))
	end
	setpos = function(pos)
		local key = minetest.pos_to_string(pos, 0)
		if db[key] then return end
		db[key] = pos
		return savedb()
	end
	rmpos = function(pos)
		local key = minetest.pos_to_string(pos, 0)
		if not db[key] then return end
		db[key] = nil
		return savedb()
	end
end

do
	local queue = {}
	minetest.register_globalstep(function()
			if #queue < 1 then
				for _, p in allpos() do
					queue[#queue + 1] = p
				end
				if #queue < 1 then return end
			end
			local pos = queue[#queue]
			queue[#queue] = nil
			local node = minetest.get_node_or_nil(pos)
			if node and (node.name ~= beaconame) then
				return rmpos(pos)
			end
		end)
end

------------------------------------------------------------------------
-- Beacon Check

local function opensky(pos)
	return minetest.get_node_light({
		x = pos.x,
		y = pos.y + 1,
		z = pos.z
	}, 0.5) == nodecore.light_sun
end

local function beaconfail(pos)
	rmpos(pos)
	return minetest.remove_node(pos)
end

local function beaconcheck(pos)
	if minetest.get_node(pos).name ~= beaconame
	then return rmpos(pos) end

	if not opensky(pos) then return beaconfail(pos) end

	for _, chk in pairs(recipenodes) do
		local chkp = {
			x = pos.x + (chk.x or 0),
			y = pos.y + (chk.y or 0) - 1,
			z = pos.z + (chk.z or 0)
		}
		if (not vector.equals(chkp, pos))
		and chk.match
		and (not nodecore.match(chkp, chk.match))
		then return beaconfail(pos) end
	end

	setpos(pos)
	nodecore.digparticles(amaldef, {
			amount = 50,
			time = 2,
			minpos = {x = pos.x - 0.5, y = pos.y - 7/16, z = pos.z - 0.5},
			maxpos = {x = pos.x + 0.5, y = pos.y - 7/16, z = pos.z + 0.5},
			minvel = {x = 0, y = 0, z = 0},
			maxvel = {x = 0, y = 0, z = 0},
			minacc = {x = 0, y = 5, z = 0},
			maxacc = {x = 0, y = 5, z = 0},
			minexptime = 1,
			maxexptime = 2,
			minsize = 4,
			maxsize = 6,
			glow = 4
		})
	return nodecore.dnt_set(pos, dntname, 2)
end

------------------------------------------------------------------------
-- Node Registration and Craft

nodecore.register_node(beaconame, {
		description = beacondesc,
		drawtype = "airlike",
		pointable = false,
		walkable = false,
		climbable = false,
		buildable_to = true,
		floodable = true,
		air_equivalent = true,
		paramtype = "light",
		sunlight_propagates = true,
		on_node_touchthru = function(pos, _, under, player)
			local raw = nodecore.touchtip_node(under, nil, player)
			if raw and under.y == pos.y - 1 then
				return raw .. "\n" .. beacondesc
			end
			return raw
		end
	})

nodecore.register_craft({
		label = "create beacon",
		action = "pummel",
		toolgroups = {thumpy = 1},
		normal = {y = 1},
		indexkeys = {"nc_igneous:amalgam"},
		check = opensky,
		nodes = recipenodes,
		after = function(pos)
			return beaconcheck({x = pos.x, y = pos.y + 1, z = pos.z})
		end
	})

------------------------------------------------------------------------
-- Recheck DNT/ABM

nodecore.register_dnt({
		name = dntname,
		nodenames = {beaconame},
		time = 2,
		autostart = true,
		action = beaconcheck
	})

------------------------------------------------------------------------
-- Skybox Mod

do
	local beaconmod = ""

	local function esc(t) return string_gsub(string_gsub(t, "%^", "\\^"), ":", "\\:") end
	local txrspot = esc("^[resize:256x256")
	local txrpattern = string_gsub(txrspot, "%W", "%%%1")

	local posscale = 128 / math_log(32768)
	nodecore.interval(1, function()
			local spawn = minetest.setting_get_pos("static_spawnpoint")
			or {x = 0, y = 0, z = 0}

			beaconmod = ""
			for _, bpos in allpos() do
				local pos = vector.subtract(bpos, spawn)
				pos.y = 0
				local dist = vector.length(pos)
				local log = math_log(dist + 1) * posscale
				pos = vector.multiply(pos, log / dist)
				local px = math_ceil(pos.z)
				local py = math_ceil(-pos.x)
				beaconmod = beaconmod .. ":" .. (123 + px) .. "," .. (123 + py)
				.. "=" .. esc("nc_beacon_star.png^[resize:11x11")
			end
		end)

	nodecore.register_playerstep({
			label = "beacon skybox",
			priority = -10,
			action = function(_, data)
				if beaconmod ~= "" then
					data.sky.textures[1] = string_gsub(
						data.sky.textures[1],
						txrpattern,
						txrspot .. beaconmod)
				end
			end
		})
end
